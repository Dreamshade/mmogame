#pragma once
#ifndef _VEC2_HPP_
#define _VEC2_HPP_


template<class T>
struct vec2 {
    union {
        T x;
        T width;
    };
    union {
        T y;
        T height;
    };
};
typedef vec2<int> vec2i;
typedef vec2<float> vec2f;

#endif _VEC2_HPP_
