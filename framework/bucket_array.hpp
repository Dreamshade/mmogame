#pragma once
#ifndef _BUCKET_ARRAY_HPP_
#define _BUCKET_ARRAY_HPP_

#include "framework.hpp"
#include <vector>


template <typename T>
struct bucket_array {
    private:
    struct bucket_ptr {
        private:
        int index;
        uint gen;

        public:
        // T* operator->();
    };

    struct item_t {
        uint generation;
        bool deleted;
        union {
            T item;
            int next_free;
        };

        item_t();
    };
    std::vector<item_t> buckets;

    public:
    bucket_array();

    T& operator[](int index);

    int add(T const& item);
    void remove(int index);
    bool was_removed(int index);
    size_t size();
};

// template <typename T>
// T* bucket_array<T>::bucket_ptr::operator->() {
    // if (gen == buckets[index].generation)
        // return &buckets[index].item;
    // return nullptr;
// }

template <typename T>
bucket_array<T>::item_t::item_t() { generation = 0; }

template <typename T>
bucket_array<T>::bucket_array() {
    buckets.reserve(32);
    item_t first_item;
    first_item.next_free = 0;
    first_item.deleted = true;
    buckets.emplace_back(first_item);
}

template <typename T>
T& bucket_array<T>::operator[](int index) {
    return buckets[index].item;
}

template <typename T>
int bucket_array<T>::add(T const& item) {
    if (buckets[0].next_free == 0) {
        item_t new_item;
        new_item.item = item;
        new_item.deleted = false;
        buckets.emplace_back(new_item);
        return (int)buckets.size() - 1;
    }
    int free_index = buckets[0].next_free;
    std::swap(buckets[0].next_free, buckets[free_index].next_free);
    buckets[free_index].item = item;
    buckets[free_index].deleted = false;
    return free_index;
}

template <typename T>
void bucket_array<T>::remove(int index) {
    ++buckets[index].generation;
    buckets[index].deleted = true;
    buckets[index].next_free = buckets[0].next_free;
    buckets[0].next_free = index;
}

template <typename T>
bool bucket_array<T>::was_removed(int index) { return buckets[index].deleted; }

template <typename T>
size_t bucket_array<T>::size() { return buckets.size(); }

#endif // _BUCKET_ARRAY_HPP_
