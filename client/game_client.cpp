﻿#include "game_client.hpp"
#include "renderer/renderer.hpp"


// Callback for when we detect a keyboard input event
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	switch (key) {
		case GLFW_KEY_ESCAPE:
			if (action == GLFW_PRESS)
				glfwSetWindowShouldClose(window, true);
		break;
		case GLFW_KEY_F1:
			if (action == GLFW_PRESS) {
				if (renderer::default_render_mode == renderer::mode::WIREFRAME)
					renderer::default_render_mode = renderer::mode::SHADED;
				else
					renderer::default_render_mode = renderer::mode::WIREFRAME;
			}
		break;
	}
}

int main(int argc, char* argv[]) {
	if (!glfwInit()) return -1;

	auto window = renderer::init();
	if (window == NULL) {
		glfwTerminate();
		return -1;
	}

	glfwSetKeyCallback(window, key_callback);

	// Game loop
	while (!glfwWindowShouldClose(window)) {
		renderer::draw(window);
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}
