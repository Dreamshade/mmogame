#include "renderer.hpp"
#include "shaders.hpp"
#include "mesh.hpp"


namespace renderer {
    context current_context;
    mode default_render_mode;

    vec2i framebuffer_size;

    context get_current_context() { return current_context; }
    vec2i get_framebuffer_size() { return framebuffer_size; }

    // Callback for size update of the window's framebuffer
    void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
        glViewport(0, 0, width, height);
        framebuffer_size.width = width;
        framebuffer_size.height = height;
    }

    GLFWwindow* create_window(int context_version_major, int context_version_minor) {
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, context_version_major);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, context_version_minor);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        return glfwCreateWindow(framebuffer_size.width, framebuffer_size.height, "Game", NULL, NULL);
    }

    GLFWwindow* init() {
        framebuffer_size.width = 1600;
        framebuffer_size.height = 900;

        // To support more opengl versions, we can put this
        // inside a loop and decrement the context version enum.
        // We should atleast provide support for 4.6 and 3.3.
        current_context = context::OGL46;
        auto window = create_window(4, 6);
        if (window == NULL) {
            current_context = context::OGL33;
            window = create_window(3, 3);
            if (window == NULL)
                return NULL;
        }
        glfwMakeContextCurrent(window);
        glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
        if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
            return NULL;

        glCullFace(GL_BACK); // Specify face for culling
        glFrontFace(GL_CCW); // GL_CW for clock-wise

        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        default_render_mode = mode::SHADED;
        if (!load_shaders())
            return NULL;

        create_quad(0, GL_STATIC_DRAW, true);
        create_quad(0, GL_STATIC_DRAW, true);

        return window;
    }

    void draw(GLFWwindow* window) {
        glClear(GL_COLOR_BUFFER_BIT);

        // @HACK: Crude implementation of rendering modes.
        // The problem here is that we are always unnecessarily changing
        // the state of OpenGL each frame.
        switch(default_render_mode) {
			case mode::WIREFRAME:
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            break;
			case mode::SHADED:
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            break;
        }

        // @TEMP: We should implement ranged for loops in the bucket_array.
        for (int i = 1; i < meshes.size(); ++i) {
            if (meshes.was_removed(i)) continue;
            if (meshes[i].backface_cull) glEnable(GL_CULL_FACE);
            else glDisable(GL_CULL_FACE);
            use_shader(meshes[i].shader);
            glBindVertexArray(meshes[i].vao);
            glDrawArrays(meshes[i].vertex_mode, 0, meshes[i].varray_count);
        }

        glfwSwapBuffers(window);
    }
}
