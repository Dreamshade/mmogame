#include "shaders.hpp"
#include <vector>
#include <string>
#include <mio/mmap.hpp>


namespace renderer {
    // @TEMP: Once we start compressing all shaders into the same file
    // we can store it in binary format with the amount of stored shaders,
    // so that allows us to create a static sized array.
    std::vector<uint> shader_programs;

    // @TEMP: Once we have all shaders in one file, memory map the entire file in
    // load_shaders() instead of here, and only pass the memory range in which to
    // read from.
    uint load_shader_from_file(const std::string filename, const GLenum shader_type, int* success) {
        std::error_code error;
        mio::mmap_source filemap = mio::make_mmap_source(filename, error);
        if (!filemap.is_open())
            return false;
        int file_size = (int)filemap.length();
        // @IMPROVEMENT: We can insert the opengl version into the cstr so
        // that we don't need to specify them in the files and we pass in
        // the correct version number for when we switch contexts.
        const char* cstr_file[] = { filemap.data() + '\0' };

        uint shader = glCreateShader(shader_type);
        glShaderSource(shader, 1, cstr_file, NULL);
        glCompileShader(shader);
        filemap.unmap();

        glGetShaderiv(shader, GL_COMPILE_STATUS, success);
        if (!success) {
            char info_log[512];
            glGetShaderInfoLog(shader, 512, NULL, info_log);
            // std::cout << infolog << "\n";
            return NULL;
        }
        return shader;
    }

    bool load_shaders() {
        int success;
        uint vert_shader = load_shader_from_file("res/shaders/default.vert", GL_VERTEX_SHADER, &success);
        uint frag_shader = load_shader_from_file("res/shaders/default.frag", GL_FRAGMENT_SHADER, &success);
        if (!vert_shader || !frag_shader)
            return false;

        uint shader_program = glCreateProgram();
        glAttachShader(shader_program, vert_shader);
        glAttachShader(shader_program, frag_shader);
        glLinkProgram(shader_program);
        glGetShaderiv(shader_program, GL_LINK_STATUS, &success);
        if (!success) {
            char info_log[512];
            glGetProgramInfoLog(shader_program, 512, NULL, info_log);
            // std::cout << "Failed to link shader.\n" << info_log << '\n';
            return false;
        }
        shader_programs.emplace_back(shader_program);
        return true;
    }

    void use_shader(uint index) {
        glUseProgram(shader_programs[index]);
    }
}
