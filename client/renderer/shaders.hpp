#pragma once
#ifndef _SHADERS_HPP_
#define _SHADERS_HPP_

#include "game_client.hpp"


namespace renderer {
    bool load_shaders();
    void use_shader(uint index);
}

#endif // _SHADERS_HPP_