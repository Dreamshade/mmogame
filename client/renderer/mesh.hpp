#pragma once
#ifndef _MESHES_HPP_
#define _MESHES_HPP_

#include "game_client.hpp"
#include "bucket_array.hpp"


namespace renderer {
    struct mesh {
        uint vao;
        uint vbo;
        uint varray_count;
        GLenum vertex_mode;
        uint shader;
        bool backface_cull;
    };

    extern bucket_array<mesh> meshes;

    void delete_mesh(int index);
    int create_mesh(float vertex_array[], uint vertex_array_size, GLenum vertex_mode, uint shader, GLenum draw_mode, bool backface_cull);
    int create_quad(uint shader, GLenum draw_mode, bool backface_cull);
}

#endif // _MESHES_HPP_
