#pragma once
#ifndef _RENDERER_HPP_
#define _RENDERER_HPP_

#include "game_client.hpp"


namespace renderer {
    enum class mode {
        WIREFRAME,
        SOLID,
        TEXTURED,
        SHADED
    };
    extern mode default_render_mode;

    enum class context {
        OGL46,
        OGL33,
    };

    vec2i get_framebuffer_size();
    context get_current_context();

    GLFWwindow* init();
    void draw(GLFWwindow* window);
}

#endif // _RENDERER_HPP_
