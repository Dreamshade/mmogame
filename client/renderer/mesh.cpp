#include "mesh.hpp"


namespace renderer {
    bucket_array<mesh> meshes;

    void delete_mesh(int index) {
        glDeleteVertexArrays(1, &meshes[index].vao);
        glDeleteBuffers(1, &meshes[index].vbo);
        meshes.remove(index);
    }

    int create_mesh(float vertex_array[], uint vertex_array_size, GLenum vertex_mode, uint shader, GLenum draw_mode, bool backface_cull) {
        mesh new_mesh;
        new_mesh.varray_count = vertex_array_size / 3;
        new_mesh.vertex_mode = vertex_mode;
        new_mesh.shader = shader;
        new_mesh.backface_cull = backface_cull;

        glGenVertexArrays(1, &new_mesh.vao);
        glGenBuffers(1, &new_mesh.vbo);
        glBindVertexArray(new_mesh.vao);
        glBindBuffer(GL_ARRAY_BUFFER, new_mesh.vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float) * vertex_array_size, vertex_array, draw_mode);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(0);

        // Not necessary to unbind the buffers, so only uncomment if there's
        // weird shit going on with the vbo's
        // glBindBuffer(GL_ARRAY_BUFFER, 0);
        // glBindVertexArray(0);

        return meshes.add(new_mesh);
    }

    int create_quad(uint shader, GLenum draw_mode, bool backface_cull) {
        float vertices[] = {
            -1.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            1.0f, -1.0f, 0.0f,
        };
        return create_mesh(vertices, 12, GL_TRIANGLE_STRIP, shader, draw_mode, backface_cull);
    }
}
