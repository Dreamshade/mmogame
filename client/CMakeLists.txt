﻿# CMakeList.txt : CMake project for Game, include source and define
# project specific logic here.
#
cmake_minimum_required (VERSION 3.10)

project(game_client)

# we don't need find_package because the lib is part of the project
find_package(glfw3 CONFIG REQUIRED)
find_package(libpng CONFIG REQUIRED)
find_package(libjpeg-turbo CONFIG REQUIRED)

# Add source to this project's executable.
add_executable(${PROJECT_NAME} "game_client.cpp" "game_client.hpp" "renderer/glad.c" "renderer/renderer.hpp" "renderer/renderer.cpp" "renderer/shaders.hpp" "renderer/shaders.cpp" "renderer/mesh.hpp" "renderer/mesh.cpp")

target_include_directories(${PROJECT_NAME} PRIVATE ${PROJECT_SOURCE_DIR} "renderer/include")
target_link_libraries(${PROJECT_NAME} PRIVATE framework glfw png_static libjpeg-turbo)

# TODO: Add tests and install targets if needed.
install(TARGETS ${PROJECT_NAME}
        CONFIGURATIONS Debug
        RUNTIME DESTINATION debug/client)

install(TARGETS ${PROJECT_NAME}
        CONFIGURATIONS Release
        RUNTIME DESTINATION release/client)

install(DIRECTORY res
        CONFIGURATIONS Debug
        DESTINATION debug/client)

install(DIRECTORY res
        CONFIGURATIONS Release
        DESTINATION release/client)
