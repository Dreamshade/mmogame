if [ "$1" = "-g" ]; then
    cmake . -Bbuild/linux -DCMAKE_INSTALL_PREFIX=bin
fi
cmake --build build/linux --target install
