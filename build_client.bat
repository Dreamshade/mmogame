@echo off
if "%~1" == "-g" cmake -Bbuild/windows -DCMAKE_TOOLCHAIN_FILE="E:/vcpkg/scripts/buildsystems/vcpkg.cmake" -DVCPKG_TARGET_TRIPLET=x64-windows-static -DCMAKE_INSTALL_PREFIX=bin -A x64
cmake --build build/windows --target install
@echo on
